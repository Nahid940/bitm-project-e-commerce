<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 9:44 AM
 */
namespace App;
class DB
{
    protected $con;

    public function __construct()
    {
        try{
            $this->con=new \PDO('mysql:host=localhost;dbname=product',"root","");
        }catch (\PDOException $exp){
            return $exp->getMessage();
        }
    }

}