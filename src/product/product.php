<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 9:46 AM
 */
namespace App\product;
use App\DB;

class product extends DB
{

    private $productid;
    private $productTitle;
    private $category;
    private $price;
    private $description;
    private $image;

    /**
     * @param mixed $productTitle
     */
    public function setProductTitle($productTitle)
    {
        $this->productTitle = $productTitle;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @param mixed $productid
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;
    }



    public function insertProduct(){
        $sql="insert into product(title,category,price,description,image) values(:title,:category,:price,:description,:image)";
        $stmt=$this->con->prepare($sql);
        $stmt->bindValue(':title',$this->productTitle);
        $stmt->bindValue(':category',$this->category);
        $stmt->bindValue(':price',$this->price);
        $stmt->bindValue(':description',$this->description);
        $stmt->bindValue(':image',$this->image);

        if($stmt->execute()){
            session_start();
            $_SESSION['insert']="Product inserted";
            header('location:index.php');
        }

    }

    public function viewAllProduct(){
        $sql="select * from product";
        $stmt=$this->con->prepare($sql);
        $stmt->execute();
        $result=$stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }


    public function viewSingleProduct(){
        $sql="select * from product where id=:productid";
        $stmt=$this->con->prepare($sql);
        $stmt->bindValue(':productid',$this->productid);
        $stmt->execute();
        $res=$stmt->fetch(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function updateProduct(){

        $sql="update product set title=:title, category=:category, price=:price, description=:description, image=:image where id=:productid";

        $stmt=$this->con->prepare($sql);
        $stmt->bindValue(':productid',$this->productid);
        $stmt->bindValue(':title',$this->productTitle);
        $stmt->bindValue(':category',$this->category);
        $stmt->bindValue(':price',$this->price);
        $stmt->bindValue(':description',$this->description);
        $stmt->bindValue(':image',$this->image);
        if($stmt->execute()){
            session_start();
            $_SESSION['update']="Product details updated";
            header('location:index.php');
        }

    }

    public function deleteProduct(){
        $sql="delete from product where id=:productid";
        $stmt=$this->con->prepare($sql);
        $stmt->bindValue(':productid',$this->productid);

        if($stmt->execute()){
            session_start();
            $_SESSION['delete']="Product deleted";
            header('location:index.php');
        }

    }

}