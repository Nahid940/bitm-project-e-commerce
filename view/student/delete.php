<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 10:52 AM
 */

include_once '../../vendor/autoload.php';

$product=new \App\product\product();

if(isset($_POST['delete'])) {
    $productid = $_POST['productid'];
    $image = $_POST['previousimage'];
    unlink('../../' . $image);

    $product->setProductid($productid);
    $product->deleteProduct();
}
?>