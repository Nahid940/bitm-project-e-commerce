<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 11:23 AM
 */

include_once '../../vendor/autoload.php';
$product=new \App\product\product();
if(isset($_POST['insert'])){
    $title=$_POST['name'];
    $cetegory=$_POST['cetegory'];
    $price=$_POST['price'];
    $description=$_POST['description'];

    $productid=$_POST['productid'];

    $product->setProductid($productid);
    $product->setProductTitle($title);
    $product->setCategory($cetegory);
    $product->setPrice($price);
    $product->setDescription($description);

    $prevouimage=$_POST['previousimage'];

    $filename=$_FILES['image']['name'];

    if($filename==''){

       $product->setImage($prevouimage);
    }else {
        $filelocation = $_FILES['image']['tmp_name'];
        $div = explode('.', $filename);

        unlink("../../".$prevouimage);
        $fileExtension = strtolower(end($div));
        $uniqueName = substr(md5(time()), 0, 10) . '.'.$fileExtension;
        $uploaded_image = "upload/".$uniqueName;
        $product->setImage($uploaded_image);
        move_uploaded_file($filelocation, "../../".$uploaded_image);

    }
    $product->updateProduct();
}
?>
?>