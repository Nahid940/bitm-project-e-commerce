<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';

$productid=$_GET['productid'];

$product=new \App\product\product();
$product->setProductid($productid);

$res=$product->viewSingleProduct();

?>

<div id="page-wrapper" style="min-height: 349px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Product</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">



            <div class="panel panel-primary">
                <div class="panel-heading">
                    Basic Product Add Form
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-8 col-md-offset-2">
                            <form role="form" action="view/student/update.php" method="POST" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Product Title</label>
                                    <input name="name" class="form-control" value="<?php echo $res['title']?>">
                                </div>

                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="cetegory" class="form-control">
                                        <option>Select One</option>
                                        <option value="male" <?php if ($res['category']=='male') echo'selected' ?>>Male</option>
                                        <option value="female" <?php if ($res['category']=='female') echo'selected' ?>>Female</option>
                                        <option value="baby" <?php if ($res['category']=='baby') echo'selected' ?>>Baby</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" value="<?php echo $res['price']?>">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" rows="3" name="description" ><?php echo $res['description']?>
                                    </textarea>

                                </div>
                                <div class="form-group">
                                    <img src="<?php echo $res['image']?>" alt="">
                                    <input type="hidden" value="<?php echo $res['image']?>" name="previousimage">
                                </div>
                                <div class="form-group">
                                    <label>Upload Image</label>
                                    <input type="file" name="image"/>
                                </div>
                                <input type="hidden" name="productid" value="<?php echo $res['id']?>">
                                <button type="reset" class="btn btn-danger">Reset</button>
                                <button type="submit" class="btn btn-primary" name="insert">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>

<?php
include_once '../include/footer.php';
?>

