<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';



$product=new \App\product\product();
//$product->setProductid($product_id);


if(isset($_POST['details'])){
    $product_id=$_POST['productid'];
    $product->setProductid($product_id);
}
$res=$product->viewSingleProduct();
?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Details
                    </div>


                        <div class="panel-body">
                            <?php
//                            foreach ( $product->viewSingleProduct() as $singleData){
                            ?>
                            <span class="thumbnail">
                                <img src="<?php echo $res['image']?>">
                                <h4>Product Tittle</h4>
                                <div class="ratings">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </div>
                                <p><?php echo $res['description']?></p>
                                <hr class="line">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <p class="price"><?php echo $res['price']?></p>
                                    </div>

                                </div>
                            </span>
<!--                                --><?php //}?>
                        </div>

                    <div class="panel-footer ">
                        <div class="row">



                            <div class="col-md-8 col-md-offset-2">


                                <table>
                                    <tr>
                                        <td><form action="view/student/delete.php" method="post">
                                                <input type="hidden" value="<?php echo $res['image']?>" name="previousimage">
                                                <input type="hidden" value="<?php echo $res['id']?>" name="productid">
                                                <button type="submit" class="btn btn-danger" name="delete">Delete</button>
                                                <!--                                <a href="view/student/delete.php?productid=--><?php //echo $res['id']?><!--" class="btn btn-danger">Delete</a>-->
                                            </form>
                                        </td>

                                        <td>
                                            <a href="view/student/edit.php?productid=<?php echo $res['id']?>" class="btn btn-primary">Edit</a>
                                        </td>

                                        <td>
                                            <a href="view/student/index.php" class="btn btn-default">Back</a>
                                        </td>
                                    </tr>
                                </table>

                            </div>


                        </div>
                    </div>
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>