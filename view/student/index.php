<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';
$product=new \App\product\product();

?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


        <div class="row">

            <?php
            if(isset($_SESSION['insert'])){
                echo "<div class='alert alert-success'>Product inserted!!</div>";
                session_unset();
            }

            if(isset($_SESSION['update'])){
                echo "<div class='alert alert-success'>".$_SESSION['update']."</div>";
                session_unset();
            }

            if(isset($_SESSION['delete'])){
                echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                session_unset();
            }

                foreach ($product->viewAllProduct() as $allData){
            ?>
            <div class="col-md-3 col-sm-6">
    		<span class="thumbnail">
      			<img src="<?php echo $allData['image']?>">
      			<h4><?php echo $allData['title']?></h4>
      			<div class="ratings">
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star-empty"></span>
                </div>


      			<p><?php echo $allData['description']?> </p>
      			<hr class="line">
      			<div class="row">
      				<div class="col-md-6 col-sm-6">
      					<p class="price"><?php echo $allData['price']?></p>
      				</div>
      				<div class="col-md-6 col-sm-6">
      				 <a href="#" target="_blank" >
                         <form action="view/student/view.php" method="post">
                             <input type="hidden" name="productid" value="<?php echo $allData['id']?>">
                         <button class="btn btn-info right" name="details"> Details</button>
                         </form>
                     </a>
      				</div>

      			</div>
    		</span>
            </div>
                <?php }?>



<!--            <div class="col-md-3 col-sm-6">-->
<!--    		<span class="thumbnail">-->
<!--      			<img src="https://s12.postimg.org/655583bx9/item_1_180x200.png" alt="...">-->
<!--      			<h4>Product Tittle</h4>-->
<!--      			<div class="ratings">-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star-empty"></span>-->
<!--                </div>-->
<!--      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
<!--      			<hr class="line">-->
<!--      			<div class="row">-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      					<p class="price">$29,90</p>-->
<!--      				</div>-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      				 <a href="#" target="_blank" >	<button class="btn btn-info right" > Details</button></a>-->
<!--      				</div>-->
<!---->
<!--      			</div>-->
<!--    		</span>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6">-->
<!--    		<span class="thumbnail">-->
<!--      			<img src="https://s12.postimg.org/5w7ki5z4t/item_4_180x200.png" alt="...">-->
<!--      			<h4>Product Tittle</h4>-->
<!--      			<div class="ratings">-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star-empty"></span>-->
<!--                </div>-->
<!--      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
<!--      			<hr class="line">-->
<!--      			<div class="row">-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      					<p class="price">$29,90</p>-->
<!--      				</div>-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      				 <a href="#" target="_blank" >	<button class="btn btn-info right" > Details</button></a>-->
<!--      				</div>-->
<!---->
<!--      			</div>-->
<!--    		</span>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6">-->
<!--    		<span class="thumbnail">-->
<!--      			<img src="https://s12.postimg.org/dawwajl0d/item_3_180x200.png" alt="...">-->
<!--      			<h4>Product Tittle</h4>-->
<!--      			<div class="ratings">-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star-empty"></span>-->
<!--                </div>-->
<!--      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
<!--      			<hr class="line">-->
<!--      			<div class="row">-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      					<p class="price">$29,90</p>-->
<!--      				</div>-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      				 <a href="#" target="_blank" >	<button class="btn btn-info right" > Details</button></a>-->
<!--      				</div>-->
<!---->
<!--      			</div>-->
<!--    		</span>-->
<!--            </div>-->
<!---->
<!--            <div class="col-md-3 col-sm-6">-->
<!--    		<span class="thumbnail">-->
<!--      			<img src="https://s12.postimg.org/41uq0fc4d/item_2_180x200.png" alt="...">-->
<!--      			<h4>Product Tittle</h4>-->
<!--      			<div class="ratings">-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star-empty"></span>-->
<!--                </div>-->
<!--      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
<!--      			<hr class="line">-->
<!--      			<div class="row">-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      					<p class="price">$29,90</p>-->
<!--      				</div>-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      				 <a href="#" target="_blank" >	<button class="btn btn-info right" > Details</button></a>-->
<!--      				</div>-->
<!---->
<!--      			</div>-->
<!--    		</span>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6">-->
<!--    		<span class="thumbnail">-->
<!--      			<img src="https://s12.postimg.org/655583bx9/item_1_180x200.png" alt="...">-->
<!--      			<h4>Product Tittle</h4>-->
<!--      			<div class="ratings">-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star-empty"></span>-->
<!--                </div>-->
<!--      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
<!--      			<hr class="line">-->
<!--      			<div class="row">-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      					<p class="price">$29,90</p>-->
<!--      				</div>-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      				 <a href="#" target="_blank" >	<button class="btn btn-info right" > Details</button></a>-->
<!--      				</div>-->
<!---->
<!--      			</div>-->
<!--    		</span>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6">-->
<!--    		<span class="thumbnail">-->
<!--      			<img src="https://s12.postimg.org/5w7ki5z4t/item_4_180x200.png" alt="...">-->
<!--      			<h4>Product Tittle</h4>-->
<!--      			<div class="ratings">-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star-empty"></span>-->
<!--                </div>-->
<!--      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
<!--      			<hr class="line">-->
<!--      			<div class="row">-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      					<p class="price">$29,90</p>-->
<!--      				</div>-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      				 <a href="#" target="_blank" >	<button class="btn btn-info right" > Details</button></a>-->
<!--      				</div>-->
<!---->
<!--      			</div>-->
<!--    		</span>-->
<!--            </div>-->
<!--            <div class="col-md-3 col-sm-6">-->
<!--    		<span class="thumbnail">-->
<!--      			<img src="https://s12.postimg.org/dawwajl0d/item_3_180x200.png" alt="...">-->
<!--      			<h4>Product Tittle</h4>-->
<!--      			<div class="ratings">-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star"></span>-->
<!--                    <span class="glyphicon glyphicon-star-empty"></span>-->
<!--                </div>-->
<!--      			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
<!--      			<hr class="line">-->
<!--      			<div class="row">-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      					<p class="price">$29,90</p>-->
<!--      				</div>-->
<!--      				<div class="col-md-6 col-sm-6">-->
<!--      			 <a href="#" target="_blank" >	<button class="btn btn-info right" > Details</button></a>-->
<!--      				</div>-->
<!---->
<!--      			</div>-->
    		</span>
            </div>
            <!-- END PRODUCTS -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>