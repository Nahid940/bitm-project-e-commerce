<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 9:50 AM
 */
include_once '../../vendor/autoload.php';
$product=new \App\product\product();
if(isset($_POST['insert'])){
    $title=$_POST['name'];
    $cetegory=$_POST['cetegory'];
    $price=$_POST['price'];
    $description=$_POST['description'];

    $filename=$_FILES['image']['name'];
    $filelocation=$_FILES['image']['tmp_name'];
    $div=explode('.',$filename);

    $fileExtension=strtolower(end($div));
    $uniqueName=substr(md5(time()), 0, 10).'.'.$fileExtension;
    $uploaded_image = "upload/".$uniqueName;
    move_uploaded_file($filelocation,"../../".$uploaded_image);


    $product->setProductTitle($title);
    $product->setCategory($cetegory);
    $product->setPrice($price);
    $product->setDescription($description);
    $product->setImage($uploaded_image);

    $product->insertProduct();

}
?>


